import { mount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import { NoteStars } from '@/components'

Vue.use(Vuetify)

describe('Stars.vue', function () {
  let wrapper

  beforeEach(() => {
    wrapper = mount(NoteStars, {
      propsData: {
        comment: {
          note: {
            accueil: 4,
            ambiance: 4,
            organisation: 4,
            deco: 4
          }
        }
      }
    })
  })

  it('should have a score of 4/5', () => {
    expect(wrapper.text()).toMatch('4/5')
    expect(wrapper.vm.total).toBe(4)
  })

  it('should have 4 stars', () => {
    expect(wrapper.html()).toContain('<i aria-hidden="true" class="v-icon material-icons theme--light orange--text">star</i>')
  })

  it('should have 1 empty star', () => {
    expect(wrapper.html()).toContain('<i aria-hidden="true" class="v-icon material-icons theme--light">star_border</i>')
  })
})
