module.exports = {
  pwa: {
    name: 'DATW',
    themeColor: '#1565c0',
    msTileColor: '#FFFFFF',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js'
    }
  }
}
