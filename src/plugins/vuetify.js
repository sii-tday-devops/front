import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#1565C0',
    secondary: '#0D47A1',
    accent: '#607D8B',
    error: '#D32F2F',
    warning: '#FBC02D',
    info: '#2196f3',
    success: '#4caf50'
  }
})
