import NoteStars from './Stars.vue'
import Camera from './Camera.vue'
import Comment from './Comment.vue'

export { NoteStars, Camera, Comment }
