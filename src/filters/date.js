import moment from 'moment'

export function timeAgo (date) {
  if (!date) {
    return ''
  }
  return moment(date).fromNow()
}

export function formatDate (date, format) {
  return moment(String(date)).format(format || 'DD/MM/YYYY HH:mm')
}
