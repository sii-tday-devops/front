import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import moment from 'moment'

Vue.use(Vuex)

const http = axios.create({
  baseURL: 'http://51.38.38.152:1664/api/',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

export default new Vuex.Store({
  state: {
    establishments: []
  },
  getters: {
    allEstablishments: (state) => state.establishments,
    getMonths: (state) => state.establishments
      .reduce((months, establishment) => {
        const month = moment(establishment.createdAt).startOf('month').toISOString()
        return months.includes(month) ? months : [...months, month]
      }, [])
      .sort()
      .reverse()
  },
  mutations: {
    SET_ESTABLISHMENTS (state, establishments) {
      state.establishments = establishments
    },
    ADD_ESTABLISHMENT (state, establishment) {
      state.establishments = [...state.establishments, establishment]
    },
    UPDATE_ESTABLISHMENT (state, establishment) {
      const index = state.establishments.findIndex(e => e.id === establishment.id)
      state.establishments.splice(index, 1, establishment)
    },
    REMOVE_ESTABLISHMENT (state, id) {
      const index = state.establishments.findIndex(e => e.id === id)
      state.establishments.splice(index, 1)
    }
  },
  actions: {
    // Get data from API and store them in the store
    async getEstablishments ({ commit }) {
      const { data: establishments } = await http.get('/establishments')
      commit('SET_ESTABLISHMENTS', establishments)

      return establishments
    },

    // Get an establishment from the store or fetch it over API
    async getEstablishment ({ state }, id) {
      let establishment = state.establishments.find(e => e.id === id)

      if (!establishment) {
        const { data } = await http.get(`/establishments/${id}`)
        establishment = data
      }

      return establishment
    },

    // Update the establishment in API then update the store
    async saveEstablishment ({ commit }, establishment) {
      if (!establishment.id) {
        const { data } = await http.post(`/establishments`, establishment)
        commit('ADD_ESTABLISHMENT', data)
        return data
      } else {
        const { data } = await http.put(`/establishments/${establishment.id}`, establishment)
        commit('UPDATE_ESTABLISHMENT', data)
        return data
      }
    },

    // Update the establishment in API then remove from the store
    async removeEstablishment ({ commit }, id) {
      const { status } = await http.delete(`/establishments/${id}`)
      const success = status === 204
      if (success) {
        commit('REMOVE_ESTABLISHMENT', id)
      }
      return success
    },

    async addComment ({ commit, state }, comment) {
      const { data } = await http.post(`/comments`, comment)

      if (data) {
        comment.id = data.id
        let establishment = state.establishments.find(e => e.id === comment.establishement)
        establishment.comments.push(comment)
        commit('UPDATE_ESTABLISHMENT', establishment)
      }

      return comment
    }
  }
})
