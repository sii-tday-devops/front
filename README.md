[![coverage report](https://gitlab.com/sii-tday-devops/front/badges/master/coverage.svg)](https://sii-tday-devops.gitlab.io/front/)

# Devops all the way - Front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
